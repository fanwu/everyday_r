require './person'

# Actually Facility is just a tilot
class Facility
  def initialize
    @occupier = nil
    @duration = 0
  end

  def occupied?
    not @occupier.nil?
  end

  def occupy(person)
    if occupied?
      false
    else
      @occupier = person
      @duration = 1
      Person.population.delete person
      true
    end
  end

  #上完走人
  def vacate
    Person.population << @occupier
    @occupier = nil
  end

  #到点上完走人
  def tick
    if occupied? and @duration > @occupier.use_duration
      vacate
      @duration = 0
    elsif occupied?
      @duration += 1
    end
  end
end