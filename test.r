library(ggplot2)

data<-read.table("demo1.csv", header=TRUE, sep=",")
mean<-mean(data)
median <- apply(data,2, median)
max <- apply(data, 2, max)
df <- data.frame(population=seq(from=10, to=600, by=10), mean=mean, median=median, max=max)
